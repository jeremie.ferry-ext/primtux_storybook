import type { StorybookConfig } from "@storybook/web-components-vite";

const config: StorybookConfig = {
  stories: [
    "../src/**/*.mdx", 
    "../src/**/*.stories.@(js|jsx|mjs|ts|tsx)"],
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@chromatic-com/storybook",
    'storybook-dark-mode',
  ],
  staticDirs: [
    {
      from: '../src/styles/design-system-variables.css',
      to: 'styles/design-system-variables.css',
    },
    {
      from: '../src/styles/design-system-semantic-light.css',
      to: 'styles/design-system-semantic-light.css',
    },
    {
      from: '../src/styles/design-system-semantic-dark.css',
      to: 'styles/design-system-semantic-dark.css',
    },
    {
      from: '../src/styles/design-system.css',
      to: 'styles/design-system.css',
    },
  ],
  core: {
    builder: '@storybook/builder-vite',
  },
  framework: {
    name: "@storybook/web-components-vite",
    options: {},
  },
};
export default config;
