import { html, css, LitElement } from 'lit';

export class PcApp extends LitElement {
    static get properties() {
        return {
            app_id: Number,
            app_icon_path: String,
            app_name: String,
            is_installed: Boolean,
        };
    }

    constructor() {
        super();
        this.is_installed = false;
    }

    static get styles() {
        return [
            css`
            .app__element {
                position: relative;
                display: flex;
                flex-direction: column;
                align-items: center;
                width: var(--spacing-12x);
            }

            .app__element--open .loader {
                animation: fadeInLoader 2.4s ease-out, mulShdSpin 1.3s infinite linear;
            }

            .app__element--open .app__img {
                animation: fadeInApp 2.4s ease-out;
            }
            
            @keyframes fadeInApp {
                0% {
                    opacity: 1;
                }  
                10%, 90% {
                    opacity: 0.3;
                }  
                100% {
                    opacity: 1;
                }
            }
            
            .app__input {
                appearance: none;
            }

            .app__element:hover {
                cursor: pointer;
            }

            .app__name {
                margin-top: var(--spacing-1x);
                color: var(--colors-text-default);
                width: 10rem; 
                text-align: center;
                font-size: 0.875rem;
                overflow: hidden;
                text-overflow: ellipsis;
                white-space: nowrap;
            }     

            .app-details__title {
                margin: .5rem;
                vertical-align: middle;
                line-height: 48px;
                display: inline-flex;
            }

            .app-details__text-title {
                padding: 0 0.5rem;
            }

            
            .app__img {
                width: var(--spacing-12x);
                height: var(--spacing-12x);
                background-color: var(--colors-background-alt);
                padding: var(--spacing-2x);
                border-radius: var(--radius-md);
                box-sizing: border-box;
                border: var(--button-border-thickness) solid transparent;
            }

            .app__input:focus + .app__img {
                border-style: dashed;
                border-color: var(--colors-border-active);
            }

            .app__input:hover + .app__img {
                border-style: solid;
                border-color: var(--colors-border-hover);
            }

            .app__input:active + .app__img {
                border-style: solid;
                border-color: var(--colors-border-active);
            }

            .app-details__img {
                height: 48px;
                vertical-align: bottom;
            }

            .app-details__description,
                .app-details__license-container,
                .app-details__link-container {
                    padding: .5rem 0;
            }

            .app__is-installed {
                padding: .5rem;
                position: absolute;
                right: -0.3rem;
                bottom: 1.1rem;
            }

            .app__is-installed:after {
                content: "\\2714";
                color: var(--colors-border-active);
            }
            `
        ]
    }

    render() {
        let is_installed_element = '';
        if (this.is_installed) {
            is_installed_element = html`<span class="app__is-installed"></span>`;
        }
        return html`
            <label class="app__element" title="${this.app_name}">
                <input type="checkbox" class="app__input" name="app-${this.app_id}" id="app-${this.app_id}" onchange="" />
                <img class="app__img" loading="lazy" src="${this.app_icon_path}" />
                <span class="loader"></span>
                <span class="app__name">${this.app_name}</span>
                ${is_installed_element}
            </label>
       `;
    }
}

window.customElements.define('pc-app', PcApp);