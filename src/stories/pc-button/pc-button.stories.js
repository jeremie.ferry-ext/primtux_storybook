import './pc-button.js';

import dark_theme_switch__svg from '../assets/svg/dark-theme-switch.svg';
import light_theme_switch__svg from '../assets/svg/light-theme-switch.svg';
import logout__svg from '../assets/svg/logout.svg';
import cancel__svg from '../assets/svg/cancel.svg';

export default {
    tags: ['autodocs'],
    title: 'Composants : Atomes/<pc-button>',
    component: 'pc-button',
    argTypes: {
        label: { type: 'string' },
        icon_path: {
            control: { type: 'select' },
            options: [
              dark_theme_switch__svg,
              light_theme_switch__svg,
              logout__svg,
              cancel__svg,
            ],
        },
    }
};

export const logout = {
    args: {
        label: 'Quitter',
        icon_path: logout__svg,
        css_classes: 'button--secondary'
    },
};

export const cancel = {
    args: {
        label: 'Annuler',
        icon_path: cancel__svg
    },
};

export const dark = {
    args: {
        icon_path: dark_theme_switch__svg
    },
};

export const light = {
    args: {
        icon_path: light_theme_switch__svg
    },
};