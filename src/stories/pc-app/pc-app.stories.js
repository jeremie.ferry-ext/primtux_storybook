import './pc-app.js';

import achat_app from '../assets/apps/achats.png';

export default {
    tags: ['autodocs'],
    title: 'Composants : Atomes/<pc-app>',
    component: 'pc-app',
    argTypes: {
        app_name: { type: 'string' },
        app_icon_path: {
            control: { type: 'select' },
            options: [
                achat_app
            ],
        },
        is_installed: { type: 'boolean' },
    }
};

export const primary = {
    args: {
        app_name: 'Achats',
        app_icon_path: achat_app
    },
};