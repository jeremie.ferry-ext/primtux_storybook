import './pc-searchbar.js';

export default {
    tags: ['autodocs'],
    title: 'Composants : Atomes/<pc-searchbar>',
    component: 'pc-searchbar'
};

export const primary = {
    args: {
    },
};